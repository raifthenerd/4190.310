Require Export Assignment12_04.

(* problem #05: 20 points *)

(** Translate this informal recursive definition into one using [fix]:
<<
   halve = 
     \x:Nat. 
        if x=0 then 0 
        else if (pred x)=0 then 0
        else 1 + (halve (pred (pred x))))
>>
*)

Definition halve : tm :=
  tfix
    (tabs A (TArrow TNat TNat)
          (tabs X TNat
                (tif0 (tvar X)
                      (tnat 0)
                      (tif0 (tpred (tvar X))
                            (tnat 0)
                            (tsucc (tapp (tvar A) (tpred (tpred (tvar X))))))))).

Example halve_type: empty |- halve \in TArrow TNat TNat.
Proof.
  unfold halve; eauto 10.
Qed.

Example halve_10: tapp halve (tnat 10) ==>* tnat 5.
Proof.
  unfold halve; normalize.
Qed.

Example halve_11: tapp halve (tnat 11) ==>* tnat 5.
Proof.
  unfold halve; normalize.
Qed.


Theorem halve_correct: forall n,
   tapp halve (tnat (n+n)) ==>* tnat n.
Proof.
  unfold halve. induction n.
  - simpl.
    eapply multi_step. apply ST_AppFix; constructor.
    eapply multi_step. apply ST_App1. apply ST_AppAbs. constructor. constructor.
    eapply multi_step. simpl. apply ST_AppAbs. constructor.
    eapply multi_step. simpl. apply ST_If0Zero.
    constructor.
  - simpl. rewrite (plus_comm n (S n)). simpl.
    eapply multi_step. apply ST_AppFix; constructor.
    eapply multi_step. apply ST_App1. apply ST_AppAbs. constructor. constructor.
    eapply multi_step. simpl. apply ST_AppAbs. constructor.
    eapply multi_step. simpl. apply ST_If0Nonzero.
    eapply multi_step. apply ST_If01. apply ST_PredNat.
    eapply multi_step. simpl. apply ST_If0Nonzero.
    eapply multi_step. apply ST_Succ1. apply ST_App2. constructor. constructor.
                       constructor. apply ST_PredNat.
    eapply multi_step. simpl. apply ST_Succ1. apply ST_App2. constructor. constructor.
                       apply ST_PredNat.
    simpl. apply multi_trans with (y:=(tsucc (tnat n))). induction IHn; eauto.
    eapply multi_step. apply ST_SuccNat. constructor.
Qed.

(*-- Check --*)

Check conj halve_type (conj halve_10 halve_11) :
  empty |- halve \in TArrow TNat TNat /\
  tapp halve (tnat 10) ==>* tnat 5 /\ 
  tapp halve (tnat 11) ==>* tnat 5.

Check halve_correct: forall n,
   tapp halve (tnat (n + n)) ==>* tnat n.

