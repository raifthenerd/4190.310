Require Export Assignment05_32.

(* problem #33: 10 points *)

Theorem le_plus_l : forall a b,
  a <= a + b.
Proof.
  intros. rewrite plus_comm. (*?*) induction b.
  - simpl. apply le_n.
  - simpl. apply le_S. apply IHb.
Qed.

