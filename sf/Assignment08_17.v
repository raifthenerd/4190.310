Require Export Assignment08_16.

(* problem #17: 10 points *)

Lemma optimize_0plus_bexp_sound:
  btrans_sound optimize_0plus_bexp.
Proof.
  assert (H:forall a st, aeval st a = aeval st (optimize_0plus_aexp a)).
    apply optimize_0plus_aexp_sound.
  unfold btrans_sound. intros. unfold bequiv. intros.
  induction b; simpl; try reflexivity;
  try (rewrite (H a st); rewrite (H a0 st); reflexivity).
  rewrite IHb. reflexivity.
  rewrite IHb1. rewrite IHb2. reflexivity.
Qed.

(*-- Check --*)
Check optimize_0plus_bexp_sound:
  btrans_sound optimize_0plus_bexp.

