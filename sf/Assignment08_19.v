Require Export Assignment08_18.

(* problem #19: 10 points *)

Lemma constfold_0plus_sound:
  ctrans_sound constfold_0plus.
Proof.
  unfold ctrans_sound. unfold constfold_0plus. intros.
  unfold cequiv. split.
  - intros.
    apply fold_constants_com_sound in H.
    apply optimize_0plus_com_sound in H.
    assumption.
  - intros.
    apply fold_constants_com_sound.
    apply optimize_0plus_com_sound.
    assumption.
Qed.

(*-- Check --*)
Check constfold_0plus_sound:
  ctrans_sound constfold_0plus.
