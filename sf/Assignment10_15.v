Require Export Assignment10_14.

(* problem #15: 10 points *)

(* Hint: 

   First study the chapter "Auto.v".

   Using [;], [try] and [eauto], you can prove it in 7 lines thanks to:. 
     Hint Constructors bstep.

   You can use the following intro pattern:
     destruct ... as [[? | ?] | [? ?]].
*)

Theorem bexp_strong_progress: forall st b,
  (b = BTrue \/ b = BFalse) \/
  exists b', b / st ==>b b'.
Proof.
  intros; induction b; try (right;
  assert (Ha:=(aexp_strong_progress st a));
  assert (Ha0:=(aexp_strong_progress st a0));
  destruct Ha; destruct Ha0; try destruct H; try destruct H0; subst; eauto);
  try (right; destruct IHb; destruct H; subst; eauto);
  try (right; destruct IHb1; destruct IHb2; destruct H; destruct H0; subst; eauto);
  try (left; eauto).
Qed.

(*-- Check --*)
Check bexp_strong_progress: forall st b,
  (b = BTrue \/ b = BFalse) \/
  exists b', b / st ==>b b'.

