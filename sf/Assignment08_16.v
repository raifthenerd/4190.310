Require Export Assignment08_15.

(* problem #16: 10 points *)

Lemma optimize_0plus_aexp_sound:
  atrans_sound optimize_0plus_aexp.
Proof.
  unfold atrans_sound. intros. unfold aequiv. intros.
  induction a; try reflexivity.
  destruct a1; destruct a2; try destruct n; try destruct n0;
  simpl in *; try rewrite IHa1; try rewrite IHa2; try omega.
  simpl; rewrite IHa1; rewrite IHa2; reflexivity.
  simpl; rewrite IHa1; rewrite IHa2; reflexivity.
Qed.

(*-- Check --*)
Check optimize_0plus_aexp_sound:
  atrans_sound optimize_0plus_aexp.

