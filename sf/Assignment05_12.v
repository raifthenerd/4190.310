Require Export Assignment05_11.

(* problem #12: 10 points *)




(** 2 stars (false_beq_nat)  *)
Theorem false_beq_nat : forall n m : nat,
     n <> m ->
     beq_nat n m = false.
Proof.
  induction n.
  - destruct m.
    + intros. simpl.
      assert (O_eq_O: 0=0). reflexivity.
      apply H in O_eq_O. inversion O_eq_O.
    + intros. simpl. reflexivity.
  - destruct m.
    + intros. simpl. reflexivity.
    + intros. simpl. apply IHn. unfold not. intros.
      rewrite H0 in H. unfold not in H. apply H. reflexivity.
Qed.
(** [] *)




