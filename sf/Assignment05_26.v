Require Export Assignment05_25.

(* problem #26: 10 points *)











(** 3 star (even__ev)  
    Note that proving [even__ev] directly is hard.
    You might find it easier to prove [even__ev_strong] first
    and then prove [even__ev] using it.
*)

Lemma even__ev_strong: forall n : nat, 
  (even (pred n) -> ev (pred n)) /\ (even n -> ev n).
Proof.
  induction n.
  - split.
    + simpl. intro. apply ev_0.
    + intro. apply ev_0.
  - inversion IHn. simpl. split.
    + apply H0.
    + destruct n.
      * intros. unfold even in H1. inversion H1.
      * intro. unfold even in H1. simpl in H1. apply ev_SS. simpl in H. apply H.
        unfold even. apply H1.
Qed.
(** [] *)


