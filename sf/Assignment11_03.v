Require Export Assignment11_02.

(* problem #03: 10 points *)

(** **** Exercise: 3 stars, optional (step_deterministic)  *)
(** Using [value_is_nf], we can show that the [step] relation is
    also deterministic... *)

Theorem step_deterministic:
  deterministic step.
Proof with eauto.
  unfold deterministic. intros x y1 y2 Hy1 Hy2. generalize dependent y2.
  induction Hy1; intros; inversion Hy2; subst;
  try reflexivity; try solve by inversion; try (apply f_equal; auto).
  - apply IHHy1 in H3. rewrite H3. reflexivity.
  - assert (value (tsucc t1)). auto. apply value_is_nf in H0.
    unfold normal_form, not in H0.
    assert (exists t' : tm, tsucc t1 ==> t'). exists t1'. auto.
    apply H0 in H2. contradiction.
  - assert (value (tsucc y2)). auto. apply value_is_nf in H.
    unfold normal_form, not in H.
    assert (exists t' : tm, tsucc y2 ==> t'). exists t1'. auto.
    apply H in H1. contradiction.
  - assert (value (tsucc t1)). auto. apply value_is_nf in H0.
    unfold normal_form, not in H0.
    assert (exists t' : tm, tsucc t1 ==> t'). exists t1'. auto.
    apply H0 in H2. contradiction.
  - assert (value (tsucc t0)). auto. apply value_is_nf in H.
    unfold normal_form, not in H.
    assert (exists t' : tm, tsucc t0 ==> t'). exists t1'. auto.
    apply H in H1. contradiction.
Qed.

(*-- Check --*)
Check step_deterministic:
  deterministic step.

