Require Export Assignment11_09.

(* problem #10: 30 points *)

(** Write a type checking function [tyeq: tm -> ty -> bool].
**)

Fixpoint tycheck (t: tm) (T: ty) : bool :=
  match t with
    | ttrue | tfalse => match T with
                          | TBool => true
                          | TNat => false
                        end
    | tzero => match T with
                 | TBool => false
                 | TNat => true
               end
    | tif b t1 t2 => match (tycheck b TBool) with
                       | true => (tycheck t1 T) && (tycheck t2 T)
                       | false => false
                     end
    | tsucc t' | tpred t' => match T with
                               | TBool => false
                               | TNat => tycheck t' TNat
                             end
    | tiszero t' => match T with
                      | TBool => tycheck t' TNat
                      | TNat => false
                    end
  end.

Example tycheck_ex1:
  tycheck
    (tif (tiszero (tpred (tsucc (tsucc tzero)))) ttrue (tiszero (tsucc tzero))) 
    TBool 
  = true.
Proof. reflexivity. Qed.

Example tycheck_ex2:
  tycheck
    (tif (tiszero (tpred (tsucc (tsucc tzero)))) tzero (tiszero (tsucc tzero))) 
    TBool 
  = false.
Proof. reflexivity. Qed.

(** Prove that the type checking function [tyeq: tm -> ty -> bool] is correct.

    Hint: use the lemma [andb_prop].
**)

Check andb_prop.

Theorem tycheck_correct1: forall t T
    (TYCHK: tycheck t T = true),
  |- t \in T.
Proof.
  induction t; intros; try (destruct T;  inversion TYCHK; auto);
  destruct (tycheck t1 TBool) eqn:Hb; try solve by inversion;
  apply andb_prop in H0; inversion H0;
  apply IHt1 in Hb; apply IHt2 in H; apply IHt3 in H1;
  apply T_If; assumption.
Qed.

Theorem tycheck_correct2: forall t T
    (HASTY: |- t \in T),
  tycheck t T = true.
Proof.
  intros. induction HASTY; auto.
  simpl. rewrite IHHASTY1. rewrite IHHASTY2. rewrite IHHASTY3. auto.
Qed.

(*-- Check --*)

Check (conj tycheck_ex1 tycheck_ex2 :
 (tycheck
    (tif (tiszero (tpred (tsucc (tsucc tzero)))) ttrue (tiszero (tsucc tzero))) 
    TBool 
  = true)
 /\
 (tycheck
    (tif (tiszero (tpred (tsucc (tsucc tzero)))) tzero (tiszero (tsucc tzero))) 
    TBool 
  = false)).

Check tycheck_correct1: forall t T
    (TYCHK: tycheck t T = true),
  |- t \in T.

Check tycheck_correct2: forall t T
    (HASTY: |- t \in T),
  tycheck t T = true.
