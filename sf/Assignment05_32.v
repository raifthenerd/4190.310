Require Export Assignment05_31.

(* problem #32: 10 points *)

Theorem Sn_le_Sm__n_le_m : forall n m,
  S n <= S m -> n <= m.
Proof. 
  induction n.
  - induction m.
    + intro. apply le_n.
    + intro. apply le_S. apply IHm. inversion H. apply H2.
  - induction m.
    + intro. inversion H. inversion H2.
    + intro. inversion H.
      * apply le_n.
      * apply le_S. apply IHm. apply H2.
Qed.

