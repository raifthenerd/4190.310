Require Export Assignment10_09.

(* problem #10: 10 points *)

(** The fact that small-step reduction implies big-step is now
    straightforward to prove, once it is stated correctly. 

    The proof proceeds by induction on the multi-step reduction
    sequence that is buried in the hypothesis [normal_form_of t t']. *)
(** Make sure you understand the statement before you start to
    work on the proof.  *)

(** **** Exercise: 3 stars (multistep__eval)  *)
Theorem multistep__eval : forall t t',
  normal_form_of t t' -> exists n, t' = C n /\ t || n.
Proof.
  assert (strong_progress: forall t, value t \/ exists t', t ==> t').
  {
    induction t.
    - left. constructor.
    - right. inversion IHt1.
      + inversion IHt2.
        * inversion H. inversion H0. exists (C (n + n0)). constructor.
        * inversion H0. exists (P t1 x). apply ST_Plus2; assumption.
      + inversion H. exists (P x t2). apply ST_Plus1; assumption.
  }
  assert (nf_is_value : forall t, step_normal_form t -> value t).
  {
    unfold step_normal_form, normal_form. intros.
    assert (tmp: value t \/ exists t', t==>t'). apply strong_progress.
    inversion tmp. assumption. apply ex_falso_quodlibet. apply H. assumption.
  }
  unfold normal_form_of, step_normal_form. intros t t' [Ht Ht'].
  apply nf_is_value in Ht'. inversion Ht'. exists n. split. reflexivity.
  induction Ht; subst. constructor.
  eapply step__eval. apply H0. apply IHHt; auto.
Qed.

(*-- Check --*)
Check multistep__eval : forall t t',
  normal_form_of t t' -> exists n, t' = C n /\ t || n.

