Require Export Assignment12_05.

(* problem #06: 10 points *)

Theorem tloop_diverges:
  ~ exists t, tapp tloop (tnat 0) ==>* t /\ normal_form step t.
Proof.
  remember (tapp tloop (tnat 0)) as t0.
  remember (tapp (tapp (tabs Loop (TArrow TNat TNat)
                             (tabs X TNat
                                   (tapp (tvar Loop) (tvar X))))
                       tloop)
                 (tnat 0)) as t1.
  remember (tapp (tabs X TNat (tapp tloop (tvar X)))
                 (tnat 0)) as t2.
  unfold tloop in *.
  assert (Dett1: forall t, t0 ==> t -> t = t1).
  {
    rewrite Heqt0. rewrite Heqt1. intros.
    inversion H; subst; eauto. inversion H3; subst; eauto.
    inversion H1. inversion H4.
  }
  assert (Dett2: forall t, t1 ==> t -> t = t2).
  {
    rewrite Heqt1. rewrite Heqt2. intros.
    inversion H; subst; eauto. inversion H3; subst; eauto.
    inversion H4; subst; eauto. inversion H5; subst; eauto.
    inversion H1. inversion H4.
  }
  assert (Dett0: forall t, t2 ==> t -> t = t0).
  {
    rewrite Heqt2. rewrite Heqt0. intros.
    inversion H; subst; eauto. inversion H3. inversion H4.
  }
  intro. destruct H as [x [Ha Hb]].
  unfold normal_form in Hb. apply Hb. clear Hb.
  remember (fun t => t=t0 \/ t=t1 \/ t=t2).
  assert (forall ta tb, ta ==>* tb -> P ta -> P tb).
  {
    rewrite HeqP. clear HeqP. clear P.
    intros ta tb H. induction H.
    - auto.
    - intros. apply IHmulti.
      destruct H1. rewrite H1 in *. apply Dett1 in H. auto.
      destruct H1. rewrite H1 in *. apply Dett2 in H. auto.
      rewrite H1 in *. apply Dett0 in H. auto.
  }
  apply H in Ha. rewrite HeqP in Ha.
  destruct Ha. rewrite H0. rewrite Heqt0. eauto.
  destruct H0. rewrite H0. rewrite Heqt1. eauto.
  rewrite H0. rewrite Heqt2. eauto.
  rewrite HeqP. auto.
Qed.

(*-- Check --*)
Check tloop_diverges:
  ~ exists t, tapp tloop (tnat 0) ==>* t /\ normal_form step t.

