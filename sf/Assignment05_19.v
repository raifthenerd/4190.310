Require Export Assignment05_18.

(* problem #19: 10 points *)




(** 2 stars (gorgeous_sum)  *)
Theorem gorgeous_sum : forall n m,
  gorgeous n -> gorgeous m -> gorgeous (n + m).
Proof.
  intros n m H. generalize dependent m. induction H.
  - intros. simpl. apply H.
  - intros. apply IHgorgeous in H0. apply g_plus3 in H0. apply H0.
  - intros. apply IHgorgeous in H0. apply g_plus5 in H0. apply H0.
Qed.
(** [] *)


