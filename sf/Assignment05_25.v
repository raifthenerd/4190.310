Require Export Assignment05_24.

(* problem #25: 10 points *)









(** 3 stars, optional (ev_plus_plus)  *)
(** Here's an exercise that just requires applying existing lemmas.  No
    induction or even case analysis is needed, but some of the rewriting
    may be tedious. 
    Hint: You can use [plus_comm], [plus_assoc], [double_plus], [double_even], [ev_sum], [ev_ev__ev].
*)
Check plus_comm. (* forall n m : nat, n + m = m + n *)
Check plus_assoc. (* forall n m p : nat, n + (m + p) = n + m + p *)
Check double_plus. (* forall n : nat, double n = n + n *)
Check double_even. (* forall n : nat, ev (double n) *)
Check ev_sum. (* forall n m : nat, ev n -> ev m -> ev (n + m) *)
Check ev_ev__ev. (* forall n m : nat, ev (n + m) -> ev n -> ev m *)

Theorem ev_plus_plus : forall n m p,
  ev (n+m) -> ev (n+p) -> ev (m+p).
Proof.
  intros n m p. intro. intro.
  apply (ev_sum (n+m) (n+p)) in H.
  - rewrite <- (plus_assoc n m (n+p)) in H.
    rewrite (plus_assoc m n p) in H.
    rewrite (plus_comm m n) in H.
    rewrite <- (plus_assoc n m p) in H.
    rewrite (plus_assoc n n (m+p)) in H.
    rewrite <- (double_plus n) in H.
    apply (ev_ev__ev (double n) (m+p)) in H.
    + apply H.
    + apply double_even.
  - apply H0.
Qed.
(** [] *)


