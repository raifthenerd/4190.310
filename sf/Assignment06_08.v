Require Export Assignment06_07.

(* problem #08: 40 points *)

(** **** Exercise: 4 stars, advanced (pigeonhole principle)  *)
(** The "pigeonhole principle" states a basic fact about counting:
   if you distribute more than [n] items into [n] pigeonholes, some 
   pigeonhole must contain at least two items.  As is often the case,
   this apparently trivial fact about numbers requires non-trivial
   machinery to prove, but we now have enough... *)

(** First a pair of useful lemmas (we already proved these for lists
    of naturals, but not for arbitrary lists). *)

Lemma app_length : forall (X:Type) (l1 l2 : list X),
  length (l1 ++ l2) = length l1 + length l2. 
Proof. 
  intro. induction l1.
  - simpl. intro. reflexivity.
  - simpl. intro. rewrite -> IHl1. reflexivity.
Qed.

Lemma appears_in_app_split : forall (X:Type) (x:X) (l:list X),
  appears_in x l -> 
  exists l1, exists l2, l = l1 ++ (x::l2).
Proof.
  intros X x. induction l.
  - intro. inversion H.
  - intro. inversion H.
    + exists []. exists l. reflexivity.
    + apply IHl in H1. inversion H1. inversion proof. rewrite proof0.
      exists (x0::witness). exists witness0. reflexivity.
Qed.

(** Now define a predicate [repeats] (analogous to [no_repeats] in the
   exercise above), such that [repeats X l] asserts that [l] contains
   at least one repeated element (of type [X]).  *)

Inductive repeats {X:Type} : list X -> Prop :=
  | rp_here : forall (x:X) (l:list X), appears_in x l -> repeats (x::l)
  | rp_later : forall (x:X) (l:list X), repeats l -> repeats (x::l)
.

(** Now here's a way to formalize the pigeonhole principle. List [l2]
    represents a list of pigeonhole labels, and list [l1] represents
    the labels assigned to a list of items: if there are more items
    than labels, at least two items must have the same label.  This
    proof is much easier if you use the [excluded_middle] hypothesis
    to show that [appears_in] is decidable, i.e. [forall x
    l, (appears_in x l) \/ ~ (appears_in x l)].  However, it is also
    possible to make the proof go through _without_ assuming that
    [appears_in] is decidable; if you can manage to do this, you will
    not need the [excluded_middle] hypothesis. *)

Theorem pigeonhole_principle: forall (X:Type) (l1  l2:list X), 
   excluded_middle -> 
   (forall x, appears_in x l1 -> appears_in x l2) -> 
   length l2 < length l1 -> 
   repeats l1.
Proof.
   intros X l1. induction l1 as [|x l1'].
   - intros. inversion H1.
   - intros. destruct (H (appears_in x l1')).
     + apply rp_here. apply H2.
     + apply rp_later.
       assert (H3: appears_in x l2). apply H0. apply ai_here.
       apply appears_in_app_split in H3. destruct H3. destruct proof.
       apply IHl1' with (l2:=witness++witness0).
       * apply H.
       * intros. rewrite proof in H0.
         assert (H4:appears_in x0 (x::l1')). apply ai_later. apply H3.
         assert (H5:=(H0 x0)).
         apply H5 in H4.
         apply app_appears_in.
         apply appears_in_app in H4. destruct H4.
           left. apply H4.
           right. inversion H4.
             rewrite H7 in H3. unfold not in H2. apply H2 in H3. inversion H3.
             apply H7.
       * rewrite proof in H1. rewrite app_length in H1. simpl in H1.
         rewrite <- plus_n_Sm in H1. apply Sn_le_Sm__n_le_m in H1.
         rewrite <- app_length in H1. unfold lt. apply H1.
Qed.

