Require Export Assignment10_05.

(* problem #06: 10 points *)

Theorem normal_forms_unique:
  deterministic normal_form_of.
Proof.
  unfold deterministic. unfold normal_form_of.  intros x y1 y2 P1 P2.
  inversion P1 as [P11 P12]; clear P1. inversion P2 as [P21 P22]; clear P2. 
  generalize dependent y2. 
  (* We recommend using this initial setup as-is! *)
  induction P11; intros; inversion P21; subst.
  - reflexivity.
  - unfold step_normal_form, normal_form, not in P12.
    assert (contra: exists t' : tm, x ==> t'). exists y. assumption.
    apply P12 in contra. contradiction.
  - unfold step_normal_form, normal_form, not in P22.
    assert (contra: exists t' : tm, y2 ==> t'). exists y. assumption.
    apply P22 in contra. contradiction.
  - apply IHP11; try assumption.
    assert (det:=step_deterministic_alt x y y0).
    assert (goal: y=y0). apply det; assumption. rewrite goal. assumption.
Qed.

(*-- Check --*)
Check normal_forms_unique:
  deterministic normal_form_of.

