Require Export Assignment08_03.

(* problem #04: 10 points *)

(** **** Exercise: 4 stars (no_whiles_terminating)  *)
(** Imp programs that don't involve while loops always terminate.
    State and prove a theorem [no_whiles_terminating] that says this. *)

(* Hint *)
Check andb_true_iff.

Theorem no_whiles_terminate: forall c st
    (NOWHL: no_whiles c = true),
  exists st', c / st || st'.
Proof.
  induction c; intros; inversion NOWHL;
  try (apply andb_true_iff in H0; inversion H0);
  try (apply (IHc1 st) in H; inversion H).
  - exists st. constructor.
  - exists (update st i (aeval st a)). constructor. reflexivity.
  - try (apply (IHc2 x) in H1; inversion H1).
    exists x0. apply E_Seq with x; assumption.
  - try (apply (IHc2 st) in H1; inversion H1).
    destruct (beval st b) eqn:bval.
    + exists x. apply E_IfTrue; assumption.
    + exists x0. apply E_IfFalse; assumption.
Qed.

(*-- Check --*)
Check no_whiles_terminate: forall c st
    (NOWHL: no_whiles c = true),
  exists st', c / st || st'.

