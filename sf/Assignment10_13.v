Require Export Assignment10_12.

(* problem #13: 10 points *)

(** **** Exercise: 3 stars, optional  *)
Lemma par_body_n : forall n st,
  st X = 0 /\ st Y = 0 ->
  exists st',
    par_loop / st ==>c*  par_loop / st' /\ st' X = n /\ st' Y = 0.
Proof.
  intros n st [Hx Hy]. unfold par_loop. induction n.
  - exists st. split. constructor. auto.
  - inversion IHn. inversion H. inversion H1. exists (update x X (S n)). split.
    eapply multi_trans. apply H0.
    eapply multi_step. apply CS_Par2. apply CS_While.
    eapply multi_step. apply CS_Par2. apply CS_IfStep. apply BS_Eq1. apply AS_Id.
    eapply multi_step. apply CS_Par2. apply CS_IfStep. rewrite H3. apply BS_Eq.
    eapply multi_step. apply CS_Par2. simpl. apply CS_IfTrue.
    eapply multi_step. apply CS_Par2. apply CS_SeqStep. apply CS_AssStep.
      apply AS_Plus1. apply AS_Id.
    eapply multi_step. apply CS_Par2. apply CS_SeqStep. apply CS_AssStep.
      rewrite H2. apply AS_Plus.
    eapply multi_step. apply CS_Par2. apply CS_SeqStep. apply CS_Ass.
    eapply multi_step. apply CS_Par2. apply CS_SeqFinish.
    rewrite plus_comm. constructor.
    auto.
Qed.

(*-- Check --*)
Check par_body_n : forall n st,
  st X = 0 /\ st Y = 0 ->
  exists st',
    par_loop / st ==>c*  par_loop / st' /\ st' X = n /\ st' Y = 0.

