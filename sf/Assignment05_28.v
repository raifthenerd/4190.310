Require Export Assignment05_27.

(* problem #28: 30 points *)


(** 4 stars (palindromes)  *)
(** A palindrome is a sequence that reads the same backwards as
    forwards.

    - Define an inductive proposition [pal] on [list X] that
      captures what it means to be a palindrome. (Hint: You'll need
      three cases.  Your definition should be based on the structure
      of the list; just having a single constructor
        c : forall l, l = rev l -> pal l
      may seem obvious, but will not work very well.)
 
    - Prove [pal_app_rev] that 
       forall l, pal (l ++ rev l).
    - Prove [pal_rev] that 
       forall l, pal l -> l = rev l.
*)

Inductive pal {X: Type} : list X -> Prop :=
  | pal_empty : pal []
  | pal_single : forall a:X, pal [a]
  | pal_long : forall (a:X) (l:list X), pal l -> pal (a::l++[a])
.

Theorem pal_app_rev: forall (X: Type) (l: list X),
  pal (l ++ rev l).
Proof.
  assert (Ha: forall (X:Type) (x:X) (l:list X), snoc l x = l ++ [x]).
  {
    intros. generalize dependent x. induction l.
    - simpl. reflexivity.
    - simpl. intros. rewrite IHl. reflexivity.
  }
  assert (Hb: forall (X:Type) (l1 l2 l3:list X), (l1 ++ l2) ++ l3 = l1 ++ (l2 ++ l3)).
  {
    intros. induction l1.
    - simpl. reflexivity.
    - simpl. rewrite IHl1. reflexivity.
  }
  intros. induction l.
  - simpl. apply pal_empty.
  - simpl. rewrite Ha. rewrite <- Hb. apply pal_long. apply IHl.
Qed.

Theorem pal_rev: forall (X: Type) (l: list X),
  pal l -> l = rev l.
Proof.
  assert (Ha: forall (X:Type) (x:X) (l:list X), snoc l x = l ++ [x]).
  {
    intros. generalize dependent x. induction l.
    - simpl. reflexivity.
    - simpl. intros. rewrite IHl. reflexivity.
  }
  assert (Hb: forall (X:Type) (x:X) (l:list X), rev (l ++ [x]) = x :: rev l).
  {
    intros. generalize dependent x. induction l.
    - simpl. reflexivity.
    - simpl. intros. rewrite Ha. rewrite Ha. rewrite IHl. reflexivity.
  }
  intros. induction H.
  - simpl. reflexivity.
  - simpl. reflexivity.
  - simpl. rewrite Ha. rewrite Hb. rewrite <- IHpal. reflexivity.
Qed.

(** [] *)




