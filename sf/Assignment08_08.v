Require Export Assignment08_07.

(* problem #08: 10 points *)

(** **** Exercise: 3 stars (swap_if_branches)  *)
(** Show that we can swap the branches of an IF by negating its
    condition *)

Theorem swap_if_branches: forall b e1 e2,
  cequiv
    (IFB b THEN e1 ELSE e2 FI)
    (IFB BNot b THEN e2 ELSE e1 FI).
Proof.
  unfold cequiv. intros. split; destruct (beval st b) eqn:bval;
  intros; inversion H; subst;
  try (simpl in H5; rewrite bval in H5; inversion H5).
  try (apply E_IfFalse; simpl; try rewrite bval; try reflexivity; try assumption).
  try (apply E_IfTrue; simpl; try rewrite bval; try reflexivity; try assumption).
  try (apply E_IfTrue; simpl; try rewrite bval; try reflexivity; try assumption).
  try (apply E_IfFalse; simpl; try rewrite bval; try reflexivity; try assumption).
Qed.

(*-- Check --*)
Check swap_if_branches: forall b e1 e2,
  cequiv
    (IFB b THEN e1 ELSE e2 FI)
    (IFB BNot b THEN e2 ELSE e1 FI).

