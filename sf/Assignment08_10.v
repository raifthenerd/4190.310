Require Export Assignment08_09.

(* problem #10: 10 points *)

(** **** Exercise: 2 stars, optional (seq_assoc)  *)
Theorem seq_assoc : forall c1 c2 c3,
  cequiv ((c1;;c2);;c3) (c1;;(c2;;c3)).
Proof.
  unfold cequiv. intros. split; intros.
  - inversion H. inversion H2. subst.
    apply E_Seq with st'1. assumption. apply E_Seq with st'0. assumption. assumption.
  - inversion H. inversion H5. subst.
    apply E_Seq with st'1. apply E_Seq with st'0. assumption. assumption. assumption.
Qed.

(*-- Check --*)
Check seq_assoc : forall c1 c2 c3,
  cequiv ((c1;;c2);;c3) (c1;;(c2;;c3)).

