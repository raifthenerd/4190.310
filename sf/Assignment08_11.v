Require Export Assignment08_10.

(* problem #11: 10 points *)

(** **** Exercise: 2 stars (assign_aequiv)  *)
Theorem assign_aequiv : forall X e,
  aequiv (AId X) e -> 
  cequiv SKIP (X ::= e).
Proof.
  assert (forall n1 x1 x2 (st: state), st x1 = n1 -> (update st x1 n1) x2 = st x2).
  {
    intros. unfold update. destruct (eq_id_dec x1 x2); subst; reflexivity.
  }
  assert (forall (st: state) X, st = (update st X (st X))).
  {
    intros.
    apply functional_extensionality. intro.
    rewrite H; reflexivity.
  }
  unfold aequiv. unfold cequiv. intros. split; intros.
  - rewrite (H0 st' X). inversion H2. constructor. rewrite <-(H1 st'). reflexivity.
  - inversion H2. subst. rewrite <-(H1 st). simpl. rewrite <-(H0 st X). constructor.
Qed.

(*-- Check --*)
Check assign_aequiv : forall X e,
  aequiv (AId X) e -> 
  cequiv SKIP (X ::= e).

