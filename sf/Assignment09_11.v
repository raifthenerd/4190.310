Require Export Assignment09_10.

(* problem #11: 10 points *)

(** **** Exercise: 3 stars, advanced, optional (is_wp_formal)  *)
(** Prove formally using the definition of [hoare_triple] that [Y <= 4]
   is indeed the weakest precondition of [X ::= Y + 1] with respect to
   postcondition [X <= 5]. *)

Theorem is_wp_example :
  is_wp (fun st => st Y <= 4)
    (X ::= APlus (AId Y) (ANum 1)) (fun st => st X <= 5).
Proof.
  unfold is_wp, hoare_triple, assert_implies. split.
  - intros. inversion H. unfold update. simpl in *. omega.
  - intros. remember (update st X (st Y +1)) as st'.
    assert (H3: (X ::= APlus (AId Y) (ANum 1)) / st || st').
    {
      rewrite Heqst'. constructor. reflexivity.
    }
    apply H in H3. rewrite Heqst' in H3. unfold update in H3.
    simpl in H3. omega.
    assumption.
Qed.

(*-- Check --*)
Check is_wp_example :
  is_wp (fun st => st Y <= 4)
    (X ::= APlus (AId Y) (ANum 1)) (fun st => st X <= 5).

