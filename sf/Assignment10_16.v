Require Export Assignment10_15.

(* problem #16: 10 points *)

(* Hint: 

   First study the chapter "Auto.v".

   Using [;], [try] and [eauto], you can prove it in 6 lines thanks to:
     Hint Constructors cstep.
 
   You can use the following intro pattern:
     destruct ... as [ | [? [? ?]]].
*)

Theorem cimp_strong_progress : forall c st,
  c = SKIP \/ 
  exists c' st', c / st ==>c c' / st'.
Proof.
  intros. induction c; auto; right;
  try (assert (Ha:=(aexp_strong_progress st a)); destruct Ha; destruct H);
  try (assert (Hb:=(bexp_strong_progress st b)); destruct Hb; destruct H);
  try (destruct IHc1 as [ | [? [? ?]]]; destruct IHc2 as [ | [? [? ?]]]);
  subst; eauto.
Qed.

(*-- Check --*)
Check cimp_strong_progress : forall c st,
  c = SKIP \/ 
  exists c' st', c / st ==>c c' / st'.

