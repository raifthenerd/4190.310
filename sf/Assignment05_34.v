Require Export Assignment05_33.

(* problem #34: 10 points *)

Theorem plus_lt : forall n1 n2 m,
  n1 + n2 < m ->
  n1 < m /\ n2 < m.
Proof. 
 unfold lt.
 assert (plus_lt_aux: forall n1 n2 m, n1 + n2 < m -> n2 < m).
 {
   unfold lt. intros. induction n1.
   - simpl in H. apply H.
   - apply IHn1. apply le_S in H. apply Sn_le_Sm__n_le_m. simpl in H. apply H.
 }
 intros. unfold lt in plus_lt_aux. split.
 - apply (plus_lt_aux n2 n1 m). rewrite plus_comm. apply H.
 - apply (plus_lt_aux n1 n2 m). apply H.
Qed.

