Require Export Assignment09_11.

(* problem #12: 10 points *)

(** **** Exercise: 2 stars, advanced (hoare_asgn_weakest)  *)
(** Show that the precondition in the rule [hoare_asgn] is in fact the
    weakest precondition. *)

Theorem hoare_asgn_weakest : forall Q X a,
  is_wp (Q [X |-> a]) (X ::= a) Q.
Proof.
  unfold is_wp, hoare_triple, assert_implies. split.
  - intros. unfold assn_sub in H0. inversion H. rewrite <-H5. assumption.
  - intros. remember (update st X (aeval st a)) as st'.
    assert (H1: (X ::= a) / st || st').
    {
      rewrite Heqst'. constructor. reflexivity.
    }
    apply H in H1. rewrite Heqst' in H1. unfold assn_sub. assumption.
    assumption.
Qed.

(*-- Check --*)
Check hoare_asgn_weakest : forall Q X a,
  is_wp (Q [X |-> a]) (X ::= a) Q.
